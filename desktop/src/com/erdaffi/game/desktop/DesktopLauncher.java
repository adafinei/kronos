package com.erdaffi.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.erdaffi.game.Kronos;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Kronos";
		config.width = 612;
		config.height = 408;

		new LwjglApplication(new Kronos(), config);
	}
}
