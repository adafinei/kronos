package com.erdaffi.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.erdaffi.game.GameUtils;

/**
 * Created by Daffi on 08/11/2015.
 */
public class LaserShot {
    private Vector2 position;
    private Vector2 velocity;

    private boolean init = false;

    public LaserShot(){
        this.position = new Vector2();
        this.velocity = new Vector2();
    }

    /**
     * Sets the position of the laser
     * @param position
     */
    public void setPosition(final Vector2 position){
        this.position.x = position.x + GameUtils.SHIP_WIDTH;
        this.position.y = position.y + GameUtils.SHIP_HEIGHT/2;
    }

    public void setVelocity(final Vector2 velocity) {
        //ship cannot fire backwards/stop
        if(velocity.x < 0){
            this.velocity.x = GameUtils.SHIP_MAX_VELOCITY;
        }else{
            // laser speed on x axis scales according to ship's speed
            this.velocity.x = velocity.x;
            this.velocity.add(GameUtils.SHIP_MAX_VELOCITY, 0);
        }
        // laser direction on y axis follows ship direction
        this.velocity.y = velocity.y;

    }

    public boolean getInit(){
        return init;
    }

    public void setInit(boolean toInitState){
        this.init = toInitState;
    }

    public void update(float delta) {

        if( position.x >= GameUtils.GAME_WIDTH || position.x <= 0) {
            init = true;
        }
        //check on vertical border:
        if( position.y >= GameUtils.GAME_HEIGHT - GameUtils.CTRL_BG_HEIGHT || position.y <= 0 ){
            this.init = true;
        }

        position.add(velocity.cpy().scl(delta));
    }

    public Vector2 getPosition() {
        return position;
    }

    public Vector2 getVelocity() {
        return velocity;
    }
}
