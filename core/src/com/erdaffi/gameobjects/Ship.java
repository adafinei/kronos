package com.erdaffi.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.erdaffi.game.GameUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Daffi on 04/10/2015.
 */
public class Ship {

    public Vector2 position;
    public Vector2 velocity;
    public Vector2 acceleration;

    public Status status = Status.IDLE;
    public List<LaserShot> listShots = new ArrayList<LaserShot>(GameUtils.MAX_SHOTS_PER_SCREEN);

    float rotation; // For handling ship rotation
    int width;
    int height;

    public enum Status {
        IDLE, FIRE, LEFT, RIGHT, UP, DOWN, BORDER, NONE
    }

    public Ship(float x, float y, int width, int height) {
        this.width = width;
        this.height = height;
        position = new Vector2(x, y);
        velocity = new Vector2(0, 0);
        acceleration = new Vector2(0, 0);
    }

    public void update(float delta) {
        switch (this.status) {
            case FIRE:
                Gdx.app.log("Ship", "Firing...");
                // generates a fire object with the ship's current direction
                fireOneShot();
                break;
            case BORDER:
                // stop the ship
                acceleration.x = 0;
                acceleration.y = 0;
                velocity.x = 0;
                velocity.y = 0;
                break;
            case IDLE:
                acceleration.x = 0;
                acceleration.y = 0;
                slowDownAndStop();
                break;
            case RIGHT:
                acceleration.add(GameUtils.SHIP_ACCELERATION_FACTOR, 0);
                break;
            case LEFT:
                acceleration.add(-GameUtils.SHIP_ACCELERATION_FACTOR, 0);
                break;
            case UP:
                acceleration.add(0, -GameUtils.SHIP_ACCELERATION_FACTOR);
                break;
            case DOWN:
                acceleration.add(0, GameUtils.SHIP_ACCELERATION_FACTOR);
                break;
            case NONE:
                break;
        }

        // in any case cannot go too fast
        checkMaxSpeed();
        // and not outside the game area
        checkBoundaries();

        // update velocity and position
        velocity.add(acceleration.cpy().scl(delta));
        position.add(velocity.cpy().scl(delta));

    }

    /**
     * Generates one lase shot to fire, and flags it for display
     */
    private void fireOneShot() {
        if(listShots.isEmpty() || listShots.size() < GameUtils.MAX_SHOTS_PER_SCREEN){
            LaserShot l = new LaserShot();
            l.setInit(true);
            listShots.add(l);
        }

        for(LaserShot l : listShots){
            if (l.getInit()){
                // we use one existing shot, we just update its position
                l.setInit(false);
                l.setPosition(this.position);
                l.setVelocity(this.velocity);
                break;
            }
        }
    }

    /**
     * Stops the ship by smoothly slowing it down
     */
    private void slowDownAndStop() {
        if(velocity.x > 0){
            if(velocity.x - GameUtils.SHIP_VELOCITY_FACTOR < 0){
                velocity.x = 0;
            }else {
                velocity.x -= GameUtils.SHIP_VELOCITY_FACTOR;
            }

        }else if(velocity.x <= 0){
            if(velocity.x + GameUtils.SHIP_VELOCITY_FACTOR > 0){
                velocity.x = 0;
            }else {
                velocity.x += GameUtils.SHIP_VELOCITY_FACTOR;
            }
        }

        if(velocity.y > 0){
            if(velocity.y - GameUtils.SHIP_VELOCITY_FACTOR < 0){
                velocity.y = 0;
            }else {
                velocity.y -= GameUtils.SHIP_VELOCITY_FACTOR;
            }

        }else if(velocity.y <= 0){
            if(velocity.y + GameUtils.SHIP_VELOCITY_FACTOR > 0){
                velocity.y = 0;
            }else {
                velocity.y += GameUtils.SHIP_VELOCITY_FACTOR;
            }
        }
    }

    /**
     * Checks ship's max velocity
     */
    private void checkMaxSpeed() {
        if (velocity.y > GameUtils.SHIP_MAX_VELOCITY) {
            velocity.y = GameUtils.SHIP_MAX_VELOCITY;
        }

        if (velocity.x > GameUtils.SHIP_MAX_VELOCITY) {
            velocity.x = GameUtils.SHIP_MAX_VELOCITY;
        }

        if (velocity.y < -GameUtils.SHIP_MAX_VELOCITY) {
            velocity.y = -GameUtils.SHIP_MAX_VELOCITY;
        }

        if (velocity.x < -GameUtils.SHIP_MAX_VELOCITY) {
            velocity.x = -GameUtils.SHIP_MAX_VELOCITY;
        }
    }

    /**
     * Check ship's boundaries
     */
    private void checkBoundaries() {
        // add check on game horizontal border reached by the ship:
        if( position.x >= GameUtils.GAME_WIDTH - GameUtils.SHIP_WIDTH) {
            Gdx.app.log("Ship", "right border reached at position:" + position.x);
            // max right reached
            if(this.status == Status.RIGHT || this.status == Status.FIRE || this.status == Status.NONE || this.status == Status.IDLE){
                this.status = Status.BORDER;
            }
        }else if (position.x <= 0){
            if(this.status == Status.LEFT  || this.status == Status.FIRE || this.status == Status.NONE || this.status == Status.IDLE) {
                this.status = Status.BORDER;
            }
        }
        //check on vertical border:
        if( position.y >= GameUtils.GAME_HEIGHT - GameUtils.CTRL_BG_HEIGHT ){
            if(this.status == Status.DOWN || this.status == Status.FIRE || this.status == Status.NONE || this.status == Status.IDLE)
                this.status = Status.BORDER;
        }else if(position.y <= 0) {
            if(this.status == Status.UP || this.status == Status.FIRE || this.status == Status.NONE || this.status == Status.IDLE){
                this.status = Status.BORDER;
            }
        }
    }

    public float getX() {
        return position.x;
    }

    public float getY() {
        return position.y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public float getRotation() {
        return rotation;
    }


}
