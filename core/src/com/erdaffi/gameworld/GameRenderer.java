package com.erdaffi.gameworld;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.erdaffi.controls.AssetLoader;
import com.erdaffi.game.GameUtils;
import com.erdaffi.gameobjects.LaserShot;
import com.erdaffi.gameobjects.Ship;
import com.badlogic.gdx.graphics.Color;

/**
 * Created by Daffi on 04/10/2015.
 */
public class GameRenderer {

    public GameWorld world;
    public OrthographicCamera cam;
    public ShapeRenderer shapeRenderer;
    public SpriteBatch spriteBatch;


    public GameRenderer(GameWorld world){
        this.world = world;
        this.cam = new OrthographicCamera();
        // y down, viewport width, viewport height
        this.cam.setToOrtho(true, world.getGameWidth(), world.getGameHeight());
        this.spriteBatch = new SpriteBatch();
        this.spriteBatch.setProjectionMatrix(cam.combined);
        // shape render
        this.shapeRenderer = new ShapeRenderer();
        this.shapeRenderer.setProjectionMatrix(cam.combined);
    }

    /**
     * Loads & animates all the stuff
     * @param delta
     */
    public void render(float delta) {
        //Gdx.app.log("GameRenderer", "render");
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        Ship ship = this.world.getShip();
        // draw ship & game objects
        spriteBatch.begin();
        spriteBatch.enableBlending();
        spriteBatch.draw(AssetLoader.shipAnimation.getKeyFrame(delta),
                ship.getX(), ship.getY(), ship.getWidth(), ship.getHeight());
        // draw the shots
        for (LaserShot shot: ship.listShots) {
            if(!shot.getInit())
                spriteBatch.draw(AssetLoader.shipLaser, shot.getPosition().x, shot.getPosition().y);
        }


        //scrollable decoration

        // static draw buttons
        staticDrawButtons(spriteBatch);
        // close the spritebatch
        spriteBatch.end();

    }

    /**
     * Draws the control buttons and other static decorations
     * @param openedSpriteBatch the existing SpriteBatch to use
     */
    private void staticDrawButtons(SpriteBatch openedSpriteBatch) {
        GameUtils.setScreenHeight(this.world.getGameHeight());
        openedSpriteBatch.draw(AssetLoader.buttonUp, GameUtils.getMoveUpDownScrX(), GameUtils.getMoveUpScrY());
        openedSpriteBatch.draw(AssetLoader.buttonDown, GameUtils.getMoveUpDownScrX(), GameUtils.getMoveDownScrY());
        openedSpriteBatch.draw(AssetLoader.buttonLeft, GameUtils.getMoveLeftScrX(), GameUtils.getMoveLeftRightScrY());
        openedSpriteBatch.draw(AssetLoader.buttonRight, GameUtils.getMoveRightScrX(), GameUtils.getMoveLeftRightScrY());
        openedSpriteBatch.draw(AssetLoader.buttonFire, GameUtils.getShootScrX(), GameUtils.getShootScrY());
    }

    public void debugRender(){

        shapeRenderer.setAutoShapeType(true);
        shapeRenderer.begin();

        shapeRenderer.rect(GameUtils.getMoveUpDownScrX(), GameUtils.getMoveUpScrY(), GameUtils.MOVE_WIDTH, GameUtils.MOVE_HEIGHT);

        shapeRenderer.rect(1, 1, GameUtils.GAME_WIDTH, GameUtils.GAME_HEIGHT -1);

        shapeRenderer.setColor(new Color(1, 0, 0, 1));
        shapeRenderer.end();
    }
}
