package com.erdaffi.gameworld;

import com.badlogic.gdx.Gdx;
import com.erdaffi.gameobjects.LaserShot;
import com.erdaffi.gameobjects.Ship;

/**
 * Created by Daffi on 04/10/2015.
 */
public class GameWorld {


    // reference to the ship
    Ship ship;

    // reference to the other stuff

    private int gameWidth;
    private int gameHeight;

    public GameWorld(int gameWidth, int gameHeight){

        this.gameWidth = gameWidth;
        this.gameHeight = gameHeight;

        // ship is created at mid-height point
        int midPointY = (int) (gameHeight / 2);
        this.ship = new Ship(33, midPointY - 5, 50, 25);
    }

    public void update(float delta) {
        //move the ship
        this.ship.update(delta);
        //move the laser shots
        for(LaserShot l : ship.listShots){
            if(!l.getInit())
                l.update(delta);
        }


    }

    public Ship getShip(){
        return this.ship;
    }


    public int getGameWidth() {
        return gameWidth;
    }

    public int getGameHeight() {
        return gameHeight;
    }
}
