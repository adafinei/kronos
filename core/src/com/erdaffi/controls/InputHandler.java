package com.erdaffi.controls;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.erdaffi.game.GameUtils;
import com.erdaffi.gameworld.GameRenderer;
import com.erdaffi.gameworld.GameWorld;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Circle;

import static com.erdaffi.gameobjects.Ship.Status.FIRE;
import static com.erdaffi.gameobjects.Ship.Status.IDLE;
import static com.erdaffi.gameobjects.Ship.Status.UP;
import static com.erdaffi.gameobjects.Ship.Status.DOWN;
import static com.erdaffi.gameobjects.Ship.Status.LEFT;
import static com.erdaffi.gameobjects.Ship.Status.RIGHT;
import static com.erdaffi.gameobjects.Ship.Status.NONE;

/**
 * Created by Daffi on 04/10/2015.
 *
 * This class handles the inputs from the users
 * It will contain the references to the game objects
 */
public class InputHandler implements InputProcessor {

    GameWorld world;
    // we construct the handler with a reference to the renderer to scale the touch position
    GameRenderer renderer;
    Vector3 touch = new Vector3();
    Rectangle buttonUp, buttonDown, buttonLeft, buttonRight;
    Circle buttonFire;

    // multitouch handling, we want to be able to fire while controlling the ship
    int dirPointer = -1, firePointer = -1;
    enum Keys {
        UP, DOWN, LEFT, RIGHT, FIRE
    }

    //controls are treated as an Enum
    static Map<Keys, Boolean> keys = new HashMap<Keys, Boolean>();

    static {

        keys.put(Keys.LEFT, false);
        keys.put(Keys.RIGHT, false);
        keys.put(Keys.UP, false);
        keys.put(Keys.DOWN, false);
        keys.put(Keys.FIRE, false);

    };

    public InputHandler(GameRenderer gameRenderer){
        this.renderer = gameRenderer;
        this.world = gameRenderer.world;
        GameUtils.setScreenHeight(world.getGameHeight());
        this.buttonUp = new Rectangle(GameUtils.getMoveUpDownScrX(), GameUtils.getMoveUpScrY(), GameUtils.MOVE_WIDTH, GameUtils.MOVE_HEIGHT);
        this.buttonDown = new Rectangle(GameUtils.getMoveUpDownScrX(), GameUtils.getMoveDownScrY(), GameUtils.MOVE_WIDTH, GameUtils.MOVE_HEIGHT);
        this.buttonLeft = new Rectangle(GameUtils.getMoveLeftScrX(), GameUtils.getMoveLeftRightScrY(), GameUtils.MOVE_WIDTH, GameUtils.MOVE_HEIGHT);
        this.buttonRight = new Rectangle(GameUtils.getMoveRightScrX(), GameUtils.getMoveLeftRightScrY(), GameUtils.MOVE_WIDTH, GameUtils.MOVE_HEIGHT);
        // fire button is a circle: x,y,radius
        this.buttonFire = new Circle(GameUtils.getShootScrX() + GameUtils.SHOOT_WIDTH/2,
                GameUtils.getShootScrY() + GameUtils.SHOOT_WIDTH/2, GameUtils.SHOOT_WIDTH/2);
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        // touch coordinates are screen coordinates
        renderer.cam.unproject(touch.set(screenX, screenY, 0));
        getKeyPressed(touch.x, touch.y);

        if(keys.get(Keys.UP)){
            this.world.getShip().status = UP;
            this.dirPointer = pointer;
        }else if(keys.get(Keys.DOWN)){
            this.world.getShip().status = DOWN;
            this.dirPointer = pointer;
        }else if(keys.get(Keys.LEFT)){
            this.world.getShip().status = LEFT;
            this.dirPointer = pointer;
        }else if(keys.get(Keys.RIGHT)){
            this.world.getShip().status = RIGHT;
            this.dirPointer = pointer;
        }

        if(keys.get(Keys.FIRE)){
            this.world.getShip().status = FIRE;
            this.firePointer = pointer;
        }
        return true;
    }


    /**
     * Gets the key pressed based on the X and Y of the touch by creating a
     * Rectangle for each button coordinate
     * @param screenX
     * @param screenY
     * @return the equivalent key pressed if any, null otherwise
     */
    public void getKeyPressed(float screenX, float screenY){

        //create rectangles for control buttons
        if(buttonUp.contains(screenX, screenY)) {
            upPressed();
        }else if(buttonDown.contains(screenX, screenY)) {
            downPressed();
        }else if(buttonLeft.contains(screenX, screenY)) {
            leftPressed();
        }else if(buttonRight.contains(screenX, screenY)) {
            rightPressed();
        }

        //check for fire
        if(buttonFire.contains(screenX, screenY)) {
            firePressed();
        }

    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // nothing is pressed, we reset the key map
        if(pointer == dirPointer) {
            keys.put(Keys.LEFT, false);
            keys.put(Keys.RIGHT, false);
            keys.put(Keys.UP, false);
            keys.put(Keys.DOWN, false);
            this.world.getShip().status = IDLE;
            dirPointer = -1;
        }else if(pointer == firePointer){
            keys.put(Keys.FIRE, false);
            this.world.getShip().status = NONE;
            firePointer = -1;

        }

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }


    private void setObjects(){

    }

    // convenience methods here
    public void leftPressed() {
        keys.get(keys.put(Keys.LEFT, true));
    }
    public void rightPressed() {
        keys.get(keys.put(Keys.RIGHT, true));
    }
    public void upPressed() {
        keys.get(keys.put(Keys.UP, true));
    }
    public void downPressed() {
        keys.get(keys.put(Keys.DOWN, true));
    }
    public void firePressed() {
        keys.get(keys.put(Keys.FIRE, true));
    }
    public void leftReleased() {
        keys.get(keys.put(Keys.LEFT, false));
    }
    public void rightReleased() {
        keys.get(keys.put(Keys.RIGHT, false));
    }
    public void upReleased() {
        keys.get(keys.put(Keys.UP, false));
    }
    public void downReleased() {
        keys.get(keys.put(Keys.DOWN, false));
    }
    public void fireReleased() {
        keys.get(keys.put(Keys.FIRE, false));
    }
}
