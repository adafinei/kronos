package com.erdaffi.controls;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.erdaffi.game.GameUtils;

/**
 * Created by Daffi on 11/10/2015.
 */
public class AssetLoader {
    public static Texture texture;
    public static TextureRegion background;

    public static Animation shipAnimation;
    /** Various sprites for the ship, for now only ship is used */
    public static TextureRegion ship, shipDown, shipUp, shipLeft, shipRight, shipLaser;

    public static TextureRegion buttonUp, buttonDown, buttonLeft, buttonRight, buttonFire;

    public static void load() {

        texture = new Texture(Gdx.files.internal("texture.png"));
        texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        ship = new TextureRegion(texture, GameUtils.SHIP_X, GameUtils.SHIP_Y, GameUtils.SHIP_WIDTH, GameUtils.SHIP_HEIGHT);
        ship.flip(false, true);

        shipUp = new TextureRegion(texture, GameUtils.SHIP_X, GameUtils.SHIP_Y, GameUtils.SHIP_WIDTH, GameUtils.SHIP_HEIGHT);
        shipUp.flip(false, true);

        shipDown = new TextureRegion(texture, GameUtils.SHIP_X, GameUtils.SHIP_Y, GameUtils.SHIP_WIDTH, GameUtils.SHIP_HEIGHT);
        shipDown.flip(false, true);

        shipLeft = new TextureRegion(texture, GameUtils.SHIP_X, GameUtils.SHIP_Y, GameUtils.SHIP_WIDTH, GameUtils.SHIP_HEIGHT);
        shipLeft.flip(false, true);

        shipRight = new TextureRegion(texture, GameUtils.SHIP_X, GameUtils.SHIP_Y, GameUtils.SHIP_WIDTH, GameUtils.SHIP_HEIGHT);
        shipRight.flip(false, true);

        shipLaser = new TextureRegion(texture, GameUtils.LASER1_X, GameUtils.LASER1_Y, GameUtils.LASER1_WIDTH, GameUtils.LASER1_HEIGHT);
        shipLaser.flip(false, true);

        TextureRegion[] shipSprites = { ship, shipUp, shipDown, shipLeft, shipRight };
        shipAnimation = new Animation(0.06f, shipSprites);
        shipAnimation.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);

        buttonUp = new TextureRegion(texture, GameUtils.MOVE_X, GameUtils.MOVE_Y, GameUtils.MOVE_WIDTH, GameUtils.MOVE_HEIGHT);
        // Create by copying existing
        buttonDown = new TextureRegion(buttonUp);
        buttonLeft = new TextureRegion(buttonUp);
        buttonRight = new TextureRegion(buttonUp);

        buttonFire = new TextureRegion(texture, GameUtils.SHOOT_X, GameUtils.SHOOT_Y, GameUtils.SHOOT_WIDTH, GameUtils.SHOOT_HEIGHT);


    }

    public static void dispose() {
        // We must dispose of the texture when we are finished.
        texture.dispose();
    }

}
