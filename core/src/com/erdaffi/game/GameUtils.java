package com.erdaffi.game;

/**
 * Created by Daffi on 04/10/2015.
 */
public class GameUtils {

    public static final int GAME_WIDTH = 800;
    public static final int GAME_HEIGHT = 480;

    // game width and height scale with resolution
    private static  int SCREEN_WIDTH = 800;
    //height is calculated dynamically
    private static int SCREEN_HEIGHT = 480;
    // ship coordinates
    public static final int SHIP_X = 0;
    public static final int SHIP_Y = 0;
    public static final int SHIP_WIDTH = 50;
    public static final int SHIP_HEIGHT = 25;

    public static final int SHIP_ANIM_X = 57;
    public static final int SHIP_ANIM_Y = 0;

    // enemy ship coordinates
    public static final int ENEMY_SHIP_X = 112;
    public static final int ENEMY_SHIP_Y = 0;

    public static final int ENEMY_SHIP_ANIM_X = 165;
    public static final int ENEMY_SHIP_ANIM_Y = 0;

    // asteroid coordinates
    public static final int ASTEROID_X = 220;
    public static final int ASTEROID_Y = 0;
    public static final int ASTEROID_WIDTH = 27;
    public static final int ASTEROID_HEIGHT = 25;

    public static final int ASTEROID_ANIM1_X = 248;
    public static final int ASTEROID_ANIM1_Y = 0;
    public static final int ASTEROID_ANIM2_X = 275;
    public static final int ASTEROID_ANIM2_Y = 0;
    public static final int ASTEROID_ANIM3_X = 302;
    public static final int ASTEROID_ANIM3_Y = 0;

    // background coordinates
    public static final int BGD_X = 0;
    public static final int BGD_Y = 57;
    public static final int BGD_WIDTH = 1024;
    public static final int BGD_HEIGHT = 80;

    public static final int CTRL_BG_HEIGHT = 180;

    // laser shot coordinates
    public static final int LASER1_X = 0;
    public static final int LASER1_Y = 31;
    public static final int LASER1_WIDTH = 30;
    public static final int LASER1_HEIGHT = 4;

    public static final int LASER2_X = 0;
    public static final int LASER2_Y = 38;

    // control buttons coordinates
    public static final int MOVE_X = 115;
    public static final int MOVE_Y = 156;
    public static final int MOVE_WIDTH = 50;
    public static final int MOVE_HEIGHT = 50;

    public static final int SHOOT_X = 0;
    public static final int SHOOT_Y = 155;
    public static final int SHOOT_WIDTH = 100;
    public static final int SHOOT_HEIGHT = 100;

    // Game physics constants
    public static final int SHIP_VELOCITY_FACTOR = 5;
    public static final int SHIP_ACCELERATION_FACTOR = 100;
    public static final int SHIP_MAX_VELOCITY = 250;

    public static final int MAX_SHOTS_PER_SCREEN = 25;


    /**
     * Returns the predefined game width
     * @return the predefined game width
     */
    public static int getScreenWidth() {
        return SCREEN_WIDTH;
    }

    public static void setScreenWidth(float width) {
        GameUtils.SCREEN_WIDTH = Math.round(width);
    }

    /**
     * Returns the game height given the game width and the device screen resolution
     *
     * @param screenWidth
     * @param screenHeight
     * @return game height
     */
    /**public static int calculateGameHeight(float screenWidth, float screenHeight) {
        //while at it we store the height
        SCREEN_HEIGHT = Math.round(screenHeight / (screenWidth / SCREEN_WIDTH));
        return SCREEN_HEIGHT;
    }*/

    public static int getScreenHeight(){
        return SCREEN_HEIGHT;
    }

    public static void setScreenHeight(float height){
        GameUtils.SCREEN_HEIGHT = Math.round(height);
    }

    /**
     * Returns the position of control buttons on the screen
     * Control position depends on width and height of the game
     */
    public static int getMoveUpDownScrX(){
        return MOVE_WIDTH * 2;
    }

    public static int getMoveUpScrY(){
        return GAME_HEIGHT - (MOVE_HEIGHT * 3);
    }

    public static int getMoveLeftScrX(){
        return MOVE_WIDTH;
    }

    public static int getMoveLeftRightScrY(){
        return GAME_HEIGHT - (MOVE_HEIGHT * 2);
    }

    public static int getMoveRightScrX(){
        return MOVE_WIDTH * 3;
    }

    public static int getMoveDownScrY(){
        return GAME_HEIGHT - MOVE_HEIGHT * 1;
    }

    public static int getShootScrX(){
        return GAME_WIDTH - SHOOT_WIDTH - SHOOT_WIDTH/2;
    }

    public static int getShootScrY(){
        return GAME_HEIGHT - MOVE_HEIGHT * 2;
    }

}
