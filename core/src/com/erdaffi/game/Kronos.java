package com.erdaffi.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.Game;
import com.erdaffi.screens.GameScreen;
import com.erdaffi.controls.AssetLoader;

public class Kronos extends Game {
	SpriteBatch batch;
	Texture img;
	
	@Override
	public void create () {
		//batch = new SpriteBatch();
		//img = new Texture("badlogic.jpg");
		Gdx.app.log("ZBGame", "created");
		AssetLoader.load();
		GameScreen mainScreen = new GameScreen();
		this.setScreen(mainScreen);

	}

	@Override
	public void dispose() {
		super.dispose();
		AssetLoader.dispose();
	}

	//@Override
	/**public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.draw(img, 0, 0);
		batch.end();
	}*/
}
