package com.erdaffi.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.erdaffi.controls.InputHandler;
import com.erdaffi.game.GameUtils;
import com.erdaffi.gameworld.GameRenderer;
import com.erdaffi.gameworld.GameWorld;

/**
 * Created by Daffi on 04/10/2015.
 */
public class GameScreen implements Screen{

    /** the world of objects to update */
    private GameWorld world;
    /** the renderer */
    private GameRenderer renderer;

    private float runTime = 0;


    public GameScreen(){

        GameUtils.setScreenHeight(Gdx.graphics.getHeight());
        GameUtils.setScreenWidth(Gdx.graphics.getWidth());
        //this.world = new GameWorld(GameUtils.getScreenWidth(), GameUtils.getScreenHeight());
        this.world = new GameWorld(GameUtils.GAME_WIDTH, GameUtils.GAME_HEIGHT);
        this.renderer = new GameRenderer(world);

        //handle the user inputs
        Gdx.input.setInputProcessor(new InputHandler(renderer));

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        runTime += delta;
        world.update(delta);
        renderer.render(delta);

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
